import React, {Component} from 'react';
import axios from 'axios';


class PharmaList extends Component {
    constructor(props) {
        super(props);
        this.state = {pharmacies: []};
    }

    componentDidMount() {
        axios.get(`https://127.0.0.1:8000/pharma`)
          .then(pharmacy => {
            console.log(pharmacy.data);
            this.setState({ pharmacies: pharmacy.data });
          });
    }


    updatePharma(id, e) {

      e.preventDefault();
      const pharmacy = {  
        nom: this.state.nom,
        quartier: this.state.quartier,
        ville: this.state.ville,
        garde: this.state.garde
    }
      axios.put(`https://127.0.0.1:8000/pharma/${id}`, pharmacy)

      .then(res => {
    
        console.log(res);

        console.log(res.data);

  

        // const pharmacies = this.state.pharmacies.filter(pharmacy => pharmacy.id !== id);

        // this.setState({ pharmacies});
        //    .catch(err => {
        //     console.log(err)
        // })

      })

    }
    deletePharma(id, e){
     
      
      if(window.confirm('Are you sure you want to delete?')) {
        axios.delete(`https://127.0.0.1:8000/pharma/${id}`)
    
          .then(res => {
    
            console.log(res);
    
            console.log(res.data);
    
      
    
            const pharmacies = this.state.pharmacies.filter(pharmacy => pharmacy.id !== id);
    
            this.setState({ pharmacies});
            //    .catch(err => {
            //     console.log(err)
            // })
    
          })
    
        }
    
      }
    
      
    render() {
        return (
            <div>
           

            <ul> {
                this.state.pharmacies.map(pharmacy =>
                    <li key = { pharmacy.id } > 
                        <h2>Nom: {pharmacy.nom}</h2> 
                        <h2>Quartier: {pharmacy.quartier}</h2> 
                        <h2> Ville: {pharmacy.ville}</h2> 
                        <h2>Garde: {pharmacy.garde}</h2>

                  <button onClick={(e) => this.deletePharma(pharmacy.id, e)}>Delete </button>
                  <button onClick={(e) => this.updatePharma(pharmacy.id, e)}>Update </button>
                
                   
                     </li>)
                
                } 
               
            </ul>

       

        </div>
        )
      }
    }
    
    export default PharmaList;
    
    
