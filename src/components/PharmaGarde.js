import React, { useState, useEffect } from "react";

import axios from 'axios';




function PharmaGarde() {
    const [pharmacies, setPharmacies] = useState([])

    useEffect(() => {
        axios.get('https://127.0.0.1:8000/pharma-garde').then((response) => {
            console.log(response);
            setPharmacies(response.data)
        })
            .catch(err => {
                console.log(err)
            })
    }, [])

    return (
        <div>
           

            <ul>
                {pharmacies.map(pharmacy =>
                    <li key={pharmacy.id}>  
                        <h2>Nom: {pharmacy.nom}</h2> 
                        <h2>Quartier: {pharmacy.quartier}</h2> 
                        <h2> Ville: {pharmacy.ville}</h2> 
                        <h2>Garde: {pharmacy.garde}</h2>
                    </li>
                )}
            </ul>

            {
                /* <h2> Liste des Pharmacies de Garde</h2>
 
                            <ul> 
                                {
                             pharmacies.map(pharmacy =>
                            <li key = { pharmacy.id } > { pharmacy.nom }... { pharmacy.garde } </li>)
                                    } 
                            </ul>  */
            }
        </div>
    )




}
export default PharmaGarde;