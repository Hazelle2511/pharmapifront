import React from 'react';
import './Navbar.css';


function Navbar() {
    return ( 
    < section className = "navbar" >
        <a href = "/"
        className = "navbar-item" >HOME </a>

        <a href = '/Liste de pharmacies'
        className = "navbar-item" > LISTE DE PHARMACIES </a> 

        <a href = '/Liste de pharmacies de garde'
        className = "navbar-item" > LISTE DE PHARMACIES DE GARDE </a> 


        <a href = '/ADD'
        className = "navbar-item" > ADD
        </a> 
    </section >

    )
}

export default Navbar;