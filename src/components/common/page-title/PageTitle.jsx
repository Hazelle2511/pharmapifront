import React from 'react';
import './PageTitle.css';

function PageTitle(props) {
    const {title} = props;
    return (
        <div>
        <section className="title">
            {title}
        </section>
            
        </div>
    )
}

export default PageTitle;
