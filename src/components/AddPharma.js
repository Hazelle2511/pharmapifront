import React from 'react';
import axios from 'axios';

 class AddPharma extends React.Component {
    state = {
        nom: '',
        quartier: '',
        ville: '',
        garde: ''

        // this.handleChange = this.handleChange.bind(this);
        // this.handleSubmit = this.handleSubmit.bind(this);
    }


//   constructor(props){
//     super(props);
//     this.state = {
//       nom:'',
//       quartier:'',
//       ville:'',
//       garde: '',
//     };

//     this.handleSubmit = this.handleSubmit.bind(this);
//     this.handleChange = this.handleChange.bind(this);
//   }


    handleChange = event => {
        // console.log(this.state, event.target.value)
        this.state[event.target.name] = event.target.value
        this.setState(this.state);
    }

    handleSubmit = event => {
        event.preventDefault();

        const pharmacy = {  
            nom: this.state.nom,
            quartier: this.state.quartier,
            ville: this.state.ville,
            garde: this.state.garde
        }

        axios.post(`https://127.0.0.1:8000/pharma`, pharmacy)
        .then(res => {
            console.log(res);
            console.log(res.data)
            this.setState({
                nom: '',
                quartier: '',
                ville: '',
                garde: ''
              });
          
        })
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Pharmacy Nom:
                        <input type="text" name="nom" value={this.state.nom} onChange={this.handleChange}></input> 
                        Pharmacy Quartier:
                        <input type="text" name="quartier" value={this.state.quartier} onChange={this.handleChange}></input> 
                        Pharmacy Ville:
                        <input type="text" name="ville" value={this.state.ville} onChange={this.handleChange}></input> 
                        Pharmacy Garde:
                        <input type="text" name="garde"  value={this.state.garde} onChange={this.handleChange}></input> 

                    </label>
                    <button type="submit">Submit</button>
                </form>
            </div>
        )
    }
}

export default AddPharma;