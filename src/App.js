import React from "react";
import {Switch, Route} from 'react-router-dom';
import './App.css';
import PharmaList from './components/PharmaList';
import PharmaGarde from './components/PharmaGarde';
import AddPharma from './components/AddPharma';

import { Header, Footer, PageTitle } from './components/common'






function App() {

    return ( 
 <div className = 'App' >
        <Header/>
            <Switch>
               <Route path ='/Add'>
                <PageTitle title="Add"/>
                <AddPharma/>
                </Route>
                <Route path ='/Liste de Pharmacies'>
             <PageTitle title="Liste de Pharmacies"/>
                <PharmaList/>
                </Route>
                <Route path ='/Liste de Pharmacies de Garde'>
             <PageTitle title="Liste de Pharmacies de Garde"/>
                <PharmaGarde/>
                </Route>
                <Route path ='/'>
                 <PageTitle title="Home"/>  
                </Route>
                
            </Switch>
       
       

    
        <Footer/> 

 </div>
 
    )




}

export default App;